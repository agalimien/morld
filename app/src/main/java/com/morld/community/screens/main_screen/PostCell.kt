package com.morld.community.screens.main_screen

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.Send
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.morld.community.R
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

enum class PostCellType {
    IMAGE,
    VIDEO
}

@Composable
fun PostImage() {
    Image(
        painterResource(id = R.drawable.soccer),
        modifier = Modifier
            .fillMaxWidth()
            .height(300.dp),
        contentDescription = "Post Image",
        contentScale = ContentScale.FillBounds,
    )
}

@Composable
fun PostVideo() {
    Box(
        modifier = Modifier.fillMaxWidth().wrapContentHeight(),
    ) {
        AndroidView(factory = {context ->
            var view = YouTubePlayerView(context)
            val fragment = view.addYouTubePlayerListener(
                object : AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        super.onReady(youTubePlayer)
                        youTubePlayer.loadVideo("lVfJa0yLrzE", 0f)
                    }
                }
            )
            view
        })
    }
}

@Composable
fun PostInfoRow(modifier: Modifier) {
    Row(horizontalArrangement = Arrangement.SpaceBetween, modifier = modifier) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painterResource(id = R.drawable.fifa),
                modifier = Modifier
                    .size(30.dp)
                    .clip(shape = CircleShape),
                contentScale = ContentScale.FillBounds,
                contentDescription = "Soccer"
            )
            Column(modifier = Modifier.padding(start = 8.dp)) {
                Text(text = "FIFA", color = Color.Black, fontSize = 10.sp)
                Text(text = "FIFA 原始音訊", color = Color.Black, fontSize = 10.sp)
            }
        }
        Icon(
            Icons.Filled.MoreVert,
            modifier = Modifier
                .size(24.dp)
                .clickable { Log.i("[PostMore_Click]", "Clicked") },
            tint = Color.White,
            contentDescription = "More"
        )
    }
}

@Composable
fun  PostFunctionRow() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Icon(
            Icons.Outlined.FavoriteBorder,
            modifier = Modifier
                .size(24.dp)
                .clickable { },
            tint = Color.White,
            contentDescription = "Like"
        )
        Spacer(modifier = Modifier.width(8.dp))
        Icon(
            painterResource(id = R.drawable.comment),
            modifier = Modifier
                .size(24.dp)
                .clickable { },
            tint = Color.White,
            contentDescription = "Like"
        )
        Spacer(modifier = Modifier.width(8.dp))
        Icon(
            Icons.Outlined.Send,
            modifier = Modifier
                .size(24.dp)
                .clickable { },
            tint = Color.White,
            contentDescription = "Like"
        )
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painterResource(id = R.drawable.bookmark),
            modifier = Modifier
                .size(24.dp)
                .clickable { },
            tint = Color.White,
            contentDescription = "Like"
        )
    }
}

@Composable
fun SpannableText(user: String) {
    val annotatedString = buildAnnotatedString {
        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, color = Color.White)) {
            append(user)
        }
        withStyle(style = SpanStyle(color = Color.White.copy(alpha = 0.8f))) {
            append(" 信仰驅動非凡 自信稱霸賽場")
        }
    }

    Text(text = annotatedString)
}

@Composable
fun PostDescription() {
    Column(modifier = Modifier.padding(start = 8.dp, end = 8.dp, bottom = 8.dp)) {
        Text(text = "2,500個讚", color = Color.White, fontWeight = FontWeight.Bold)
        SpannableText(user = "FIFA")
        ClickableText(text = buildAnnotatedString {
            withStyle(style = SpanStyle(color = Color.White.copy(alpha = 0.5f))) {
                append("查看全部100則留言")
            }
        }, onClick = { Log.i("[PostAllMsgs]", "Clicked") })
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PostLeaveMsg() {
    var inputText by remember { mutableStateOf("") }

    Row(verticalAlignment = Alignment.CenterVertically) {
        Image(
            painterResource(id = R.drawable.autumn),
            modifier = Modifier
                .size(30.dp)
                .clip(shape = CircleShape),
            contentScale = ContentScale.FillBounds,
            contentDescription = "Autumn"
        )
        TextField(
            modifier = Modifier
                .fillMaxWidth(),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
            ),
            value = inputText,
            textStyle = TextStyle(color = Color.White),
            maxLines = 1,
            onValueChange = { inputText = it },
            placeholder = { Text("新增留言......", fontSize = 16.sp) }
        )
    }
}

@Composable
fun PostCell(cellType: PostCellType) {
    Column {
        Box(modifier = Modifier.fillMaxWidth()) {
            if (cellType == PostCellType.VIDEO) PostVideo() else PostImage()
            PostInfoRow(modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
                .align(Alignment.TopStart))
        }
        PostFunctionRow()
        PostDescription()
        PostLeaveMsg()
    }
}