package com.morld.community.screens.main_screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.morld.community.screens.main_screen.model.NavItem
import com.morld.community.screens.main_screen.sub_screen.AddSubScreen
import com.morld.community.screens.main_screen.sub_screen.CommunitySubScreen
import com.morld.community.screens.main_screen.sub_screen.HomeSubScreen
import com.morld.community.screens.main_screen.sub_screen.LiveStreamSubScreen
import com.morld.community.screens.main_screen.sub_screen.StoreSubScreen

@Composable
fun MainBottomNavigation(appNavHostController: NavHostController, navController: NavHostController) {
    NavHost(navController, startDestination = NavItem.Home.route) {
        composable(NavItem.Home.route) {
            HomeSubScreen()
        }
        composable(NavItem.Community.route) {
            CommunitySubScreen(appNavHostController)
        }
        composable(NavItem.Add.route) {
            AddSubScreen()
        }
        composable(NavItem.LiveStream.route) {
            LiveStreamSubScreen()
        }
        composable(NavItem.Store.route) {
            StoreSubScreen()
        }
    }
}