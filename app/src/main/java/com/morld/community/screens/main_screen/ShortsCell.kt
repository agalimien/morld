package com.morld.community.screens.main_screen

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import com.morld.community.navigation.AppNavItem
import com.morld.community.screens.main_screen.model.ShortsVideoItem

@Composable
fun ShortsCell(shortsTitle: String, item: ShortsVideoItem, appHostController: NavHostController) {
    Column(
        modifier = Modifier
            .wrapContentSize()
            .clickable {
                Log.i("[ShortsCell]", "Click")
                appHostController.navigate("${AppNavItem.PlayShorts.route}/${item.videoId}")
           },
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(modifier = Modifier
            .width(70.dp)
            .height(140.dp)
            .clip(RoundedCornerShape(16.dp))
        ) {
            AsyncImage(
                modifier = Modifier.fillMaxHeight(),
                model = item.thumbnailLink,
                contentDescription = "YT Video",
                contentScale = ContentScale.Crop
            )
            Icon(Icons.Outlined.Face,
                contentDescription = null,
                tint = Color(0xFFd1a919),
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .padding(2.dp))
        }
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = shortsTitle, color = Color(0xFFd1a919), fontSize = 14.sp, textAlign = TextAlign.Center)
    }
}