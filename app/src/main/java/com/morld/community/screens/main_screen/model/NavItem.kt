package com.morld.community.screens.main_screen.model

import com.morld.community.R

const val HOME_ROUTE = "home"
const val COMMUNITY_ROUTE = "community"
const val ADD_ROUTE = "add"
const val LIVE_STREAM_ROUTE = "liveStream"
const val STORE_ROUTE = "store"

sealed class NavItem(val route: String, val icon: Int, val title: String? = null) {
    object Home : NavItem(route = HOME_ROUTE, icon = R.drawable.home_icon, title = "首頁")
    object Community : NavItem(route = COMMUNITY_ROUTE, icon = R.drawable.community_icon, title = "社群")
    object Add : NavItem(route = ADD_ROUTE, icon = R.drawable.add_icon)
    object LiveStream : NavItem(route = LIVE_STREAM_ROUTE, icon = R.drawable.livestream_icon, title = "直播")
    object Store : NavItem(route = STORE_ROUTE, icon = R.drawable.strore_icon, title = "商城")
}
