package com.morld.community.screens.main_screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.morld.community.R

@Composable
fun TopBar() {
    Image(
        painterResource(id = R.drawable.top_bar_img),
        contentDescription = "Morld",
        modifier = Modifier.size(width = 161.dp, height = 45.dp),
        contentScale = ContentScale.Fit
    )
}