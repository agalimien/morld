package com.morld.community.screens.main_screen.model

data class ShortsVideoItem(val ytUrl: String) {
    val videoId: String
        get() = ytUrl.split("v=")[1]
    val thumbnailLink: String
        get() {
            val videoId = ytUrl.split("v=")[1]
            //https://stackoverflow.com/questions/43894658/how-to-get-video-thumbnail-from-youtube-url-and-set-it-to-image-view-in-android
            //http://img.youtube.com/vi/{ID}/mqdefault.jpg
            return  "https://img.youtube.com/vi/${videoId}/mqdefault.jpg"
        }
}

val shortsList = listOf<ShortsVideoItem>(
    ShortsVideoItem("https://www.youtube.com/watch?v=LmrKejHOaG4"),
    ShortsVideoItem("https://www.youtube.com/watch?v=L1BTJsDAWjo"),
    ShortsVideoItem("https://www.youtube.com/watch?v=MzLZXUnekuI"),
    ShortsVideoItem("https://www.youtube.com/watch?v=R0C-S9ZOhzE"),
    ShortsVideoItem("https://www.youtube.com/watch?v=5Oqfnlu8VBU"),
    ShortsVideoItem("https://www.youtube.com/watch?v=20uf1EcGqjY"),
    ShortsVideoItem("https://www.youtube.com/watch?v=ZlAvdm7QMfQ"),
    ShortsVideoItem("https://www.youtube.com/watch?v=glJexaQx9C0"),
    ShortsVideoItem("https://www.youtube.com/watch?v=dVgsBeTmeNc"),
    ShortsVideoItem("https://www.youtube.com/watch?v=FyvnNgSLHRI"),
)