package com.morld.community.screens.main_screen.sub_screen

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.morld.community.screens.main_screen.PostCell
import com.morld.community.screens.main_screen.PostCellType
import com.morld.community.screens.main_screen.ShortsVideoRow

@Composable
fun CommunitySubScreen(appNavHostController: NavHostController) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxWidth()
            .background(color = Color(0xFF000000))
    ) {
        ShortsVideoRow(appNavHostController)
        Divider(color = Color(0xFF3b3d3d), thickness = 1.dp)
        PostCell(cellType = PostCellType.VIDEO)
        PostCell(cellType = PostCellType.IMAGE)
    }
}