package com.morld.community.navigation

const val MAIN = "home"
const val PLAY_SHORTS = "community"

sealed class AppNavItem(val route: String) {
    object Main : AppNavItem(route = MAIN)
    object PlayShorts : AppNavItem(route = PLAY_SHORTS)
}