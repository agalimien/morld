package com.morld.community.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.morld.community.screens.main_screen.MainScreen
import com.morld.community.screens.play_shorts_screen.PlayShortsScreen

@Composable
fun AppNavigation(navController: NavHostController) {
    NavHost(navController, startDestination = AppNavItem.Main.route) {
        composable(AppNavItem.Main.route) {
            MainScreen(navController)
        }
        composable(
            "${AppNavItem.PlayShorts.route}/{videoId}",
            arguments = listOf(navArgument("videoId") { type = NavType.StringType })
        ) {backStackEntry ->
            PlayShortsScreen(backStackEntry.arguments?.getString("videoId") ?: "")
        }
    }
}