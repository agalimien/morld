package com.morld.community

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import com.morld.community.navigation.AppNavigation
import com.morld.community.screens.main_screen.MainScreen
import com.morld.community.ui.theme.MorldCommunityTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MorldCommunityTheme {
                // A surface container using the 'background' color from the theme
                val appNavHostController = rememberNavController()
                AppNavigation(navController = appNavHostController)
            }
        }
    }
}